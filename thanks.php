<!DOCTYPE html>
<html class="no-js pattern_1">
<head>
<title>Contact Us</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Lato:300,400,700&amp;subset=latin,latin-ext"/>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-responsive.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/reset.css"/>
<link id="color_css" rel="stylesheet" type="text/css" href="css/color_scheme_1.css"/>
<link rel="stylesheet" type="text/css" href="css/jquery.combosex.css"/>
<link rel="stylesheet" type="text/css" href="css/jquery.flexslider.css"/>
<link rel="stylesheet" type="text/css" href="css/jquery.scrollbar.css"/>

<!--[if (lte IE 9)]>
    <link rel="stylesheet" type="text/css" href="css/iefix.css"/>
    <![endif]-->
<script type="text/javascript" src="js/jquery.1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.1.7.2.min.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="js/jquery.combosex.min.js"></script>
<script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="js/jquery.easytabs.min.js"></script>
<script type="text/javascript" src="js/jquery.gmap.min.js"></script>
<script type="text/javascript" src="js/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="js/fitvids.js"></script><!-- fIt Video -->
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
</head>
<body>

<!-- Bar -->
<?php include ('topheader.php'); ?>
<!-- /Bar --> 

<!-- Header -->
<?php include ('header.php'); ?>
<!-- /Header --> 

<!-- Content -->
<div id="content">
  <div id="title">
    <h1 class="inner title-2">Contact Us
      <ul class="breadcrumb-inner">
        <li> <a href="index.php">Home</a></li>
        <li> <a href="thanks.php">Thanks</a></li>
      </ul>
    </h1>
  </div>
  <div class="inner"> 
    
    <!-- Content Inner -->
    <div class="content-inner"> 
      
      <!-- Content Center -->
      <div class="content-center">
          <p>Message Successfully Send</p>
       </div>
      <!-- /Content Center --> 
      
      <!-- Content Right -->
      <div class="content-right">
        <div class="block background">
          <h2 class="title-1">Send Us a Message</h2>
          <div class = "block-content">
           
            <form id ="contact" class="email" action="mailer.php" method="post">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue.</p>
              <div id = "about">
                <input title="Your Name" type="text" name="name" class="textfield2" placeholder="Name" onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
                <input title="Your E-Mail" type="text" name="email" class="textfield2" placeholder="E-mail" onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
                <input title="Your WebSite" type="text" name="subject" class="textfield2" placeholder="WebSite" onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
              </div>
              <div id = "mess">
                <textarea name="message" title="Your Message" name="message" cols="30" rows="6" class="textarea" onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');">How can I help you?</textarea>
              </div>
              <div id = "send">
                <input id="send_btn" type="submit" value="Submit">  </a>
              </div>
            </form>
          </div>

        </div>
      </div>
      <!-- /Content Right -->
      
      <div class="clear"></div>
      <!-- Clear Line --> 
      
    </div>
    <!-- /Content Inner --> 
    
  </div>
</div>
<!-- /Content --> 

<!-- Footer -->

<?php include ('footer.php'); ?>
</body>
</html>