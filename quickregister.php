<!--
  author-naresh10oct@gmail.com

-->
<?php 

include('config.php');

?>
<!DOCTYPE html>
<html class="no-js pattern_1">
<head>
<title>Post Job</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Lato:300,400,700&amp;subset=latin,latin-ext"/>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-responsive.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/reset.css"/>
<link id="color_css" rel="stylesheet" type="text/css" href="css/color_scheme_1.css"/>
<link rel="stylesheet" type="text/css" href="css/jquery.combosex.css"/>
<link rel="stylesheet" type="text/css" href="css/jquery.flexslider.css"/>
<link rel="stylesheet" type="text/css" href="css/jquery.scrollbar.css"/>

<!--[if (lte IE 9)]>
    <link rel="stylesheet" type="text/css" href="css/iefix.css"/>
    <![endif]-->
<script type="text/javascript" src="js/jquery.1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery.combosex.min.js"></script>
<script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="js/jquery.easytabs.min.js"></script>
<script type="text/javascript" src="js/jquery.gmap.min.js"></script>
<script type="text/javascript" src="js/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="js/fitvids.js"></script><!-- fIt Video -->
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
</head>
<body>

<?php include ('topheader.php'); ?>
<?php include ('header.php'); ?>

<?php
function GetImageExtension($imagename)
   {
     if(empty($imagename)) return false;
     $pos=strpos($imagename,'.');
     return substr($imagename, $pos);
   }


 if(isset($_POST['quickregister']))
 {
  if(isset($_POST['name']))
  {
    $name=$_POST['name'];
  }
  if(isset($_POST['phone']))
  {
    $phone=$_POST['phone'];
  }
  if(isset($_POST['email']))
  {
    $email=$_POST['email'];
  }
  if(isset($_POST['degree']))
  {
    $qualification=$_POST['degree'];
  }
  if(isset($_POST['branch']))
  {
    $branch=$_POST['branch'];
  }
  if(isset($_POST['passoutyear']))
  {
    $passoutyear=$_POST['passoutyear'];
  }
  if(isset($_POST['college']))
  {
    $college=$_POST['college'];
  }
  
if (!empty($_FILES["resume"]["name"])) {


    $file_name=$_FILES["resume"]["name"];
    $temp_name=$_FILES["resume"]["tmp_name"];
    $imgtype=$_FILES["resume"]["type"];
    $ext= GetImageExtension($file_name);
    $imagename=date("d-m-Y")."-".time().$ext;
    //$imagename=$_FILES["image"]["name"];
    $target_path = "uploads/".$imagename;
    
if(move_uploaded_file($temp_name, $target_path)) {
    $query="INSERT INTO nss_quickregister (Name,Mobile,Email,Qualification,Branch,PassoutYear,College,Resume) VALUES('$name','$phone','$email','$qualification','$branch','$passoutyear','$college','$target_path');";    
}

else{
echo 'Image upload failed!';
}

}

else{
    $query="INSERT INTO nss_quickregister (Name,Mobile,Email,Qualification,Branch,PassoutYear,College) VALUES('$name','$phone','$email','$qualification','$branch','$passoutyear','$college');";    

   echo("Image not found!");
} 
$result=mysql_query($query);
if(!$result)
{
    echo "Error".mysql_error();
}
else
{
    // echo 'Success';
    echo '<script type="text/javascript">
window.alert("Form Submitted Successfully");</script>';
  header("Location:index.php");
}

 }

?>


<!-- Content -->
<div id="content">
  <div id="title">
    <h1 class="inner title-2">Contact Us
      <ul class="breadcrumb-inner">
        <li> <a href="index.php">Home</a></li>
        <li> <a href="contacts.php">Post A Job</a></li>
      </ul>
    </h1>
  </div>
  <div class="inner"> 
    
    <!-- Content Inner -->
    <div class="content-inner"> 


      <!-- Content Right -->
      <div class="content-inner">
        <div class="block background">
          <h2 class="title-1">Your Details</h2>
          <div class = "block-content">

           <h3></h3>
            <form id ="" class="" action="quickregister.php" method="post" enctype="multipart/form-data">
              <p>Fill in your details, we will contact you asap.</p>
             
             <div style="width:50%;float:left;" >
              <div id = "">
                <label for="nm" >Name:</label>
                <input  id="nm" name="name" required="required" type="text" placeholder="Your Name." class="textfield2"onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
              </div>
             
              <div id = "">
                <label for="con" >Contact:</label>
                <input  id="con" name="phone" required="required" type="text" placeholder="Mobile Number" class="textfield2"onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
                </div>
              <div id = "">
                <label for="em" >Email:</label>
                <input  id="em" name="email"  required="required" type="text" placeholder="Email ID" class="textfield2" onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
                </div>
              <div id = "">
                <label for="de" >Qualification:</label>
                <input  id="de" name="degree" type="tel" placeholder="e.g. B.Sc, B.Tech" class="textfield2" onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
                </div>
              </div>
              <div style="width:50%;float:right;">
              <div id = "">
                <label for="br" >Branch/Stream:</label>
                <input  id="br" name="branch" type="text" placeholder="e.g. Computer Science" class="textfield2" onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
                </div>
              <div id = "">
                <label for="yop" >Year Of Passout:</label>
                <input  id="yop" name="passoutyear" type="text"placeholder="YYYY-MM-DD"  class="textfield2" onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
                </div>
              <div id = "">
                <label for="col" >College:</label>
                <input  id="col" name="college" type="text" placeholder=" College Name" class="textfield2" onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
                </div>
              
              <div id = "">
                <label for="res" >Resume:</label>
                <input  id="res" name="resume" type="file" class="textfield2"onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
              </div>
              </div>
            
                <div id = "">
                <label >Anything Specific:</label>
                <textarea style="width: 100%;" id="cdes" name="additionalinfo"  rows="6" class="textfield2" onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');">Any Additional Information</textarea>
                </div>
                

              <div id = "">
                <input id="" type="submit" name="quickregister" value="Update"></a>
              </div>
            </form>



          </div>
        </div>

      </div>
      <!-- /Content Right -->
      
      <div class="clear"></div>
      <!-- Clear Line --> 
      
    </div>
    <!-- /Content Inner --> 
    
  </div>
</div>
<!-- /Content --> 

<?php include ('footer.php'); ?>
</body>
</html>