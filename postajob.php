<!--
  author-naresh10oct@gmail.com
-->
<?php
session_start();
if(!isset($_SESSION['userdetails']))
{
  header("Location:login.php");
}
include('config.php');
include('queries.php');
?>
<!DOCTYPE html>
<html class="no-js pattern_1">
<head>
<title>Post Job</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Lato:300,400,700&amp;subset=latin,latin-ext"/>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-responsive.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/reset.css"/>
<link id="color_css" rel="stylesheet" type="text/css" href="css/color_scheme_1.css"/>
<link rel="stylesheet" type="text/css" href="css/jquery.combosex.css"/>
<link rel="stylesheet" type="text/css" href="css/jquery.flexslider.css"/>
<link rel="stylesheet" type="text/css" href="css/jquery.scrollbar.css"/>

<!--[if (lte IE 9)]>
    <link rel="stylesheet" type="text/css" href="css/iefix.css"/>
    <![endif]-->
<script type="text/javascript" src="js/jquery.1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery.combosex.min.js"></script>
<script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="js/jquery.easytabs.min.js"></script>
<script type="text/javascript" src="js/jquery.gmap.min.js"></script>
<script type="text/javascript" src="js/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="js/fitvids.js"></script><!-- fIt Video -->
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
</head>
<body>

<?php include ('topheader.php'); ?>
<?php include ('header.php'); ?>
<?php

if($_POST['companydetails'])
{
  //code for 

  $companyid=$_POST['companyid'];
  $companyname=$_POST['companyname'];
  $companylogo=$_POST['companylogo'];
  $companyphone=$_POST['companyphone'];
  $companyfax=$_POST['companyfax'];
  $companyemail=$_POST['companyemail'];
  $companywebsite=$_POST['companywebsite'];
  $companyindustry=$_POST['companyindustry'];
  $companydescription=$_POST['companydescription'];
  $where=array('CompanyID'=>$companyid);
  $fieldstobeupdated=array('CompanyID'=>$companyid,'CompanyName'=>$companyname,'CompanyLogo'=>$companylogo,'CompanyPhone'=>$companyphone,
    'CompanyFax'=>$companyfax ,'CompanyEmail'=>$companyemail,'CompanyWebsite'=>$companywebsite,'CompanyIndustry'=>$companyindustry,'CompanyDescription'=>$companydescription);
  $updated=update('nss_company',$fieldstobeupdated,$where);
  if(!$updated)
  {
    echo "Error:companyinformantioncannotbeupdated".mysql_error();
  }
  unset($_POST['companydetails']);

}

if(isset($_SESSION['userdetails']['id']))
  $recruiterid=$_SESSION['userdetails']['id'];
$whereclause=array('RecruiterID'=>$recruiterid);
$fields=array('CompanyID');
$selectcid=select('nss_recruiter',$whereclause);
if($selectcid)
{
  $item=mysql_fetch_array($selectcid);
  $companyid=$item['CompanyID'];
  $whereclause=array('CompanyID'=>$companyid);
  $selectall=select('nss_company',$whereclause);
  if($selectall)
  {
      $row=mysql_fetch_array($selectall);
      $companyid=$row['CompanyID'];
      $companyname=$row['CompanyName'];
      $companylogo=$row['CompanyLogo'];
      $companyphone=$row['CompanyPhone'];
      $companyfax=$row['CompanyFax'];
      $companyemail=$row['CompanyEmail'];
      $companywebsite=$row['CompanyWebsite'];
      $companyindustry=$row['CompanyIndustry'];
      $companydescription=$row['CompanyDescription'];
  }
  else{
    echo "Error:companyinformationnotfound".mysql_error();
  }
}
else{
  echo "Error:cidnotfound".mysql_error();
}



?>
<!-- Content -->
<div id="content">
  <div id="title">
    <h1 class="inner title-2">Contact Us
      <ul class="breadcrumb-inner">
        <li> <a href="index.php">Home</a></li>
        <li> <a href="contacts.php">Post A Job</a></li>
      </ul>
    </h1>
  </div>
  <div class="inner"> 
    
    <!-- Content Inner -->
    <div class="content-inner"> 
      
      <!-- Content Center 
      <div class="content-center">
        
        <div id="contacts" class="block post-box box-1 contact-address">
          <div class="block-content">
            <div class="left">
               </div>
            <div class="right">
                </div>
          </div>
        </div>
      </div>
    /Content Center --> 

      <!-- Content Right -->
      <div class="content-inner">
        <div class="block background">
          <h2 class="title-1">1. Company Details</h2>
          <div class = "block-content">

           <h3></h3>
            <form id ="" class="" action="" method="post">
              <p>Update the Company Details if you wish to or <b>skip this step</b></p>
             
             <div style="width:50%;float:left;" >
              <div id = "">
                <label for="cid" >CompanyID(autogenerated):</label>
                <input  id="cid" name="companyid" disabled="disabled" type="text" value="<?php echo $companyid; ?>" class="textfield2"onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
              </div>
             
              <div id = "">
                <label for="cn" >Company Name:</label>
                <input  id="cn" name="companyname" disabled="disabled" type="text" value="<?php echo $companyname; ?>" class="textfield2"onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
                </div>
              <div id = "">
                <label for="ci" >Company Industry:</label>
                <input  id="ci" name="companyindustry" type="text" value="<?php echo $companyindustry; ?>" class="textfield2" onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
                </div>
              <div id = "">
                <label for="cp" >Company Phone:</label>
                <input  id="cp" name="companyphone" type="tel" value="<?php echo $companyphone; ?>" class="textfield2" onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
                </div>
              </div>
              <div style="width:50%;float:right;">
              <div id = "">
                <label for="cf" >Company Fax:</label>
                <input  id="cf" name="companyfax" type="text" value="<?php echo $companyfax; ?>" class="textfield2" onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
                </div>
              <div id = "">
                <label for="ce" >Company Email:</label>
                <input  id="ce" name="companyemail" type="email" value="<?php echo $companyemail; ?>" class="textfield2" onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
                </div>
              <div id = "">
                <label for="cw" >Company Website:</label>
                <input  id="cw" name="companywebsite" type="url" value="<?php echo $companywebsite; ?>" class="textfield2" onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
                </div>
              
              <div id = "">
                <label for="cn" >Company Logo:</label>
                <input  id="cn" name="companylogo" type="file"  class="textfield2"onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
              </div>
              </div>
            
                <div id = "">
                <label >Company Description:</label>
                <textarea style="width: 100%;" id="cdes" name="companydescription"  rows="6" class="textfield2" onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');">Company Description?</textarea>
                </div>
                

              <div id = "">
                <input id="" type="submit" name="companydetails" value="Update"></a>
              </div>
            </form>



          </div>
        </div>
     

        <!--Job Details form-->
        <div class="block background">
          <h2 class="title-1">2. Job Details</h2>
          <div class = "block-content">
      
           <h3></h3>
            <form id ="" class="" action="" method="post">
              <p>Fill in the Job Details</p>
             
             <div style="width:50%;float:left;" >
              <div id = "">
                <label for="jid" >JobID:</label>
                <input  id="jid" name="jobid" disabled="disabled" type="text" required="required" value="Autogenerated" class="textfield2"onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
              </div>
              <div id = "">
                <label for="dsg" >Designation:</label>
                <input  id="dsg" name="designation" type="text" required="required" placeholder="Post Offered" class="textfield2"onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
              </div>
              <div id = "">
                <label for="sal" >Salary/Package:</label>
                <input  id="sal" name="salary" type="text" placeholder="Salary/Package offered." class="textfield2"onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
              </div>
              <div id = "">
                <label for="dom" >Domain:</label>
                <input  id="dom" name="domain" type="text" placeholder="e.g. Web Development,Android Development etc." class="textfield2"onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
              </div>
              <div id = "">
                <label for="opd" >Opening Date:</label>
                <input  id="opd" name="openingdate" type="date" placeholder="YYYY-MM-DD" class="textfield2"onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
                </div>
              <div id = "">
                <label for="cld" >Closing Date:</label>
                <input  id="cld" name="closingdate" type="date" required="YYYY-MM-DD"  class="textfield2" onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
                </div>
              
              <div id = "">
                <label for="jt" >Job Type:</label>
                <select style="height:unset;"  id="jt" name="jobtype" type="text" class="textfield2" onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');">
                  <option value="">----------</option>
                  <option value="Part Time">Part Time</option>
                  <option value="Full Time">Full Time</option>
                  <option value="Contract">Contract</option>
                  <option value="Freelancer">Freelancer</option>
                  
                </select>

                </div>
              </div>
                <div style="width:50%;float:right">
              <div id = "">
                <label for="loc" >Location:</label>
                <input  id="loc" name="location" type="text" required="required" placeholder="Place,Country" class="textfield2" onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
                </div>
              <div id = "">
                <label for="url_" >Url:</label>
                <input  id="url_" name="url" type="url"  class="textfield2" placeholder="http://www.joburloncompanywebsite.com" onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
                </div>
              <div id = "">
                <label for="pl" > Required Programming Languages:</label>
                <input  id="pl" name="planguages" type="text"  class="textfield2" onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
                </div>
             <div id = "">
                <label for="qual" >Qualification:</label>
                <input  id="qual" name="qualification" type="text" placeholder="Minimum Qulification" class="textfield2" onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
                </div>
              <div id = "">
                <label for="exp" > Required Experience:</label>
                <input  id="exp" name="experience" type="text" placeholder="Number of years" class="textfield2" onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
                </div>
              <div id = "">
                <label for="sk" >Skills:</label>
                <input  id="sk" name="skills" type="text" placeholder="skills separated by commas." class="textfield2" onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
                </div>
              <div id = "">
                <label for="tg">Tags:</label>
                <input  id="tg" name="tags" type="text" placeholder="tags separated by commas." class="textfield2" onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');"/>
                </div>
            </div>
                <div id = "">
                <label >Description:</label>
                <textarea style="width: 100%;" id="cdes" required="required"  name="description"  rows="6" class="textfield2" onclick="this.value='';" onfocus="$(this).addClass('active');" onblur="$(this).removeClass('active');">Job Description</textarea>
                </div>
                

              <div id = "">
                <input id="" type="submit" value="Update"></a>
              </div>
            </form>



          </div>
        </div>



      </div>
      <!-- /Content Right -->
      
      <div class="clear"></div>
      <!-- Clear Line --> 
      
    </div>
    <!-- /Content Inner --> 
    
  </div>
</div>
<!-- /Content --> 

<?php include ('footer.php'); ?>
</body>
</html>