<!DOCTYPE html>
<html class="no-js pattern_1">
<head>
<title>About Us</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Lato:300,400,700&amp;subset=latin,latin-ext"/>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-responsive.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/reset.css"/>
<link id="color_css" rel="stylesheet" type="text/css" href="css/color_scheme_1.css"/>
<link rel="stylesheet" type="text/css" href="css/jquery.combosex.css"/>
<link rel="stylesheet" type="text/css" href="css/jquery.flexslider.css"/>
<link rel="stylesheet" type="text/css" href="css/jquery.scrollbar.css"/>

<!--[if (lte IE 9)]>
    <link rel="stylesheet" type="text/css" href="css/iefix.css"/>
    <![endif]-->
<script type="text/javascript" src="js/jquery.1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery.combosex.min.js"></script>
<script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="js/jquery.easytabs.min.js"></script>
<script type="text/javascript" src="js/jquery.gmap.min.js"></script>
<script type="text/javascript" src="js/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="js/fitvids.js"></script><!-- fIt Video -->
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
</head>
<body>
<?php include('topheader.php');?>
<?php include('header.php');?>

<!-- Content -->
<div id="content">
  <div id="title">
    <h1 class="inner title-2">About Us
      <ul class="breadcrumb-inner">
        <li> <a href="index.php">Home</a></li>
        <li> <a href="about-us.php">About Us</a></li>
      </ul>
    </h1>
  </div>
  <div class="inner"> 
    
    <!-- Content Inner -->
    <div class="content-inner"> 
      <!-- Content Center -->
      <div class="content-center">
        <div class="body">
          <div class="clear"></div>
          <div class="heading-l">
            <h2>Hello, nice to meet you!</h2>
          </div>
          <div class="post-box box-1">
            <p><strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi.</strong></p>
            <p>Cras vel lorem. Etiam pellentesque aliquet tellus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.</p>
            <div class="video-container">
              <iframe src="http://player.vimeo.com/video/24456787" width="536" height="328" frameborder="0" ></iframe>
            </div>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi.</p>
            <p>Cras vel lorem. Etiam pellentesque aliquet tellus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.</p>
            <div id="about-us-our-team" class="block">
              <h2 class="title-1">Our Team</h2>
              <div class="block-content">
                <div class="team-worker">
                  <div class="photo"><img src="content/our-team/worker-1.jpg" height="154" width="154" alt="Jeffrey Richards - CEO"/></div>
                  <div class="name">Jeffrey Richards</div>
                  <div class="post">CEO</div>
                </div>
                <div class="team-worker">
                  <div class="photo"><img src="content/our-team/worker-2.jpg" height="154" width="154" alt="John Doe - Partner"/></div>
                  <div class="name">John Doe</div>
                  <div class="post">Partner</div>
                </div>
                <div class="team-worker">
                  <div class="photo"><img src="content/our-team/worker-3.jpg" height="154" width="154" alt="Jane Doe - Partner"/></div>
                  <div class="name">Jane Doe</div>
                  <div class="post">Partner</div>
                </div>
                <div class="team-worker">
                  <div class="photo"><img src="content/our-team/worker-4.jpg" height="154" width="154" alt="Jeffrey Richards - HR"/></div>
                  <div class="name">Jeffrey Richards</div>
                  <div class="post">HR</div>
                </div>
                <div class="team-worker">
                  <div class="photo"><img src="content/our-team/worker-5.jpg" height="154" width="154" alt="Jane Richards - HR"/></div>
                  <div class="name">Jane Richards</div>
                  <div class="post">HR</div>
                </div>
                <div class="team-worker">
                  <div class="photo"><img src="content/our-team/worker-6.jpg" height="154" width="154" alt="Jeffrey Richards - Administrator"/></div>
                  <div class="name">Jeffrey Richards</div>
                  <div class="post">Administrator</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /Content Center --> 
      
      <!-- Content Right -->
      <div class="content-right">
        <div id="about-us-navigation" class="box-1">
          <div class="">
            <ul>
              <li class="active"><a href="about-us.php">About Us</a></li>
              <li><a href="privacy-policy.php">Privacy Policy</a></li>
              <li><a href="terms-and-conditions.php">Terms and Conditions</a></li>
            </ul>
          </div>
        </div>
      </div>
      <!-- /Content Right -->
      
      <div class="clear"></div>
      <!-- Clear Line --> 
      
    </div>
    <!-- /Content Inner --> 
    
  </div>
</div>
<!-- /Content --> 

<?php include('footer.php');?>
</body>
</html>