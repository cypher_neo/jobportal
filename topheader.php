<!-- Bar -->

<div id="bar" >

    <div class="inner" >
    <!-- Language Menu -->
    <ul id="lang-menu">
      <li id="en" class="current">En</li>
      <li id="fr"><a href="#">Fr</a></li>
      <li id="de"><a href="#">De</a></li>
      <li id="it"><a href="#">It</a></li>
    </ul>
    <!-- /Language Menu --> 
    
   <!-- User Menu -->
    <ul id="user-menu">
      <li class="dropdown" style="float:right">
        <a class="dropdown-toggle" data-toggle="dropdown">My Account
        <span class="caret"></span></a>
        <ul class="dropdown-menu" style="background-color: black;">
          <li><a href="#">MyProfile</a></li>
          <li><a href="#">Jobs Applied for</a></li>
          <li><a href="#">Logout</a></li>
        </ul>
      </li>
      <?php 
      session_start();
       if (isset($_SESSION['userdetails']))
        echo '<li><a id="logoutbtn" href="logout.php"><i class="fa fa-lock"></i>Logout</a></li>';
        
       else
       echo '<li><a id="loginbtn" href="login.php"><i class="fa fa-lock"></i>Login</a></li>';            
      ?>
      <li id="register_" style="float:right"><a href="login.php#toregister">Register</a></li>
      <li id="bookmarks" style="float:right"><a href="#">Bookmarks</a></li>
   </ul>

   <!-- /User Menu --> 
    
  </div>

  
</div>
<!-- /Bar --> 
