<?php

//This function copies the row from nss_temp_job_details to nss_job_details.
//Input is TmpJobID.
function copyTJDtoJD($tjid)
{
$query="INSERT INTO nss_job_details (`RecruiterID`,`CompanyID`,`Designation`,`Salary`,`OpeningDate`,`ClosingDate`,`Active`,`Applicants`,`JobType`,`Location`,`Description`,`Url`,`Tags`,`Languages`,`Domain`,`Qualification`,`Experience`,`Skills`) SELECT ``RecruiterID`,CompanyID`,`Designation`,`Salary`,`OpeningDate`,`ClosingDate`,`Active`,`Applicants`,`JobType`,`Location`,`Description`,`Url`,`Tags`,`Languages`,`Domain`,`Qualification`,`Experience`,`Skills`FROM nss_tmp_job_details AS tjd WHERE tjd.`TmpJobID`=$tjid;";
//echo $query;
$result=mysql_query($query);
return $result;
}

function totalrecords($tablename)
{
$tablename=mysql_real_escape_string($tablename);
$query="SELECT COUNT(*) AS total FROM $tablename;";
$result=mysql_query($query);

$rows= mysql_fetch_array($result);
return $rows['total'];
}



function maxm($tablename,$columnname)
{
    $tablename=mysql_real_escape_string($tablename);
    $query='SELECT MAX('.$columnname.') AS max FROM '.$tablename;
    $result=mysql_query($query);
    $rows=mysql_fetch_array($result);
    return $rows['max'];
}

function jobdetailsInnerJoinCompany($limit,$offset)
{     $limitSQL='';
    if(!empty($limit))
    {
        $limitSQL.=" LIMIT ".$limit;

    }
    $offsetSQL='';
    if(!empty($offset))
    {
        $offsetSQL.=" OFFSET ".$offset;
    }
    $query="SELECT * FROM nss_job_details AS jd INNER JOIN nss_company AS c WHERE jd.CompanyID=c.CompanyID";
    $query.=$limitSQL.$offsetSQL;
    //echo $query;
    $result=mysql_query($query);
    return $result;
}
//$fields and $wheere_clause are left optional
function select($tablename,$fields='',$where_clause='',$limit='',$offset='')
{

	$whereSQL = '';
    if(!empty($where_clause))
    {
    	$i = 1;
        $whereSQL.=" WHERE ";
		foreach($where_clause as $key => $value) 
		{
		$whereSQL .= " `$key`=$value ";
		if ($i < count($where_clause)) 
			{ 
				// last item should not include the AND 
				$whereSQL .= 'AND';
			}
		 $i++; 
		} 
        
    }
    $limitSQL='';
    if(!empty($limit))
    {
        $limitSQL.=" LIMIT ".$limit;

    }
    $offsetSQL='';
    if(!empty($offset))
    {
        $offsetSQL.=" OFFSET ".$offset;
    }

   $fieldSQL='';
   if(!empty($fields))
   {

   	$fieldSQL=implode(',',$fields);
   }

   if(empty($fieldSQL))
   	$fieldSQL=" * ";
$sql="SELECT ".$fieldSQL." FROM ".$tablename.$whereSQL.$limitSQL.$offsetSQL;
return mysql_query($sql);
}



function insert($table_name, $form_data)
{
    // retrieve the keys of the array (column titles)
    $fields = array_keys($form_data);

    // build the query
    $sql = "INSERT INTO ".$table_name."
    (`".implode('`,`', $fields)."`)
    VALUES('".implode("','", $form_data)."')";

    // run and return the query result resource
    return mysql_query($sql);
}



// where clause is left optional
function update($table_name, $form_data, $where_clause='')
{
    // check for optional where clause
    $whereSQL = '';
    if(!empty($where_clause))
    {
    	$i = 1;
        $whereSQL.=" WHERE ";
		foreach($where_clause as $key => $value) 
		{
		$whereSQL .= $key."=".$value;
		if ($i < count($where_clause)) 
			{ 
				// last item should not include the AND 
				$whereSQL .= 'AND';
			}
		 $i++; 
		} 
        
    }


    
    // start the actual SQL statement
    $sql = "UPDATE ".$table_name." SET ";

    // loop and build the column /
    $sets = array();
    foreach($form_data as $column => $value)
    {
         $sets[] = "`".$column."` = '".$value."'";
    }
    $sql .= implode(', ', $sets);

    // append the where statement
    $sql .= $whereSQL;
    //echo $sql;
    // run and return the query result
    return mysql_query($sql);
}

// the where clause is left optional incase the user wants to delete every row!
function dbRowDelete($table_name, $where_clause='')
{
    // check for optional where clause
    $whereSQL = '';
    if(!empty($where_clause))
    {
    	$i = 1;
        $whereSQL.=" WHERE ";
		foreach($where_clause as $key => $value) 
		{
		$whereSQL .= "`$key`=$value";
		if ($i < count($where_clause)) 
			{ 
				// last item should not include the AND 
				$whereSQL .= 'AND';
			}
		 $i++; 
		} 
        
    }
    // build the query
    $sql = "DELETE FROM ".$table_name.$whereSQL;
    //echo $sql;
    // run and return the query result resource
    return mysql_query($sql);
}


?>