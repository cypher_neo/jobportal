<!DOCTYPE html>
<html class="no-js pattern_1">
<head>
<title>Candidate Listing With Sidebar</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Lato:300,400,700&amp;subset=latin,latin-ext"/>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-responsive.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/reset.css"/>
<link id="color_css" rel="stylesheet" type="text/css" href="css/color_scheme_1.css"/>
<link rel="stylesheet" type="text/css" href="css/jquery.combosex.css"/>
<link rel="stylesheet" type="text/css" href="css/jquery.flexslider.css"/>
<link rel="stylesheet" type="text/css" href="css/jquery.scrollbar.css"/>

<!--[if (lte IE 9)]>
    <link rel="stylesheet" type="text/css" href="css/iefix.css"/>
    <![endif]-->
<script type="text/javascript" src="js/jquery.1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.1.7.2.min.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="js/jquery.combosex.min.js"></script>
<script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="js/jquery.easytabs.min.js"></script>
<script type="text/javascript" src="js/jquery.gmap.min.js"></script>
<script type="text/javascript" src="js/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
</head>
<body>
<?php include('topheader.php');?>
<?php include('header.php');?>


<!-- Content -->
<div id="content"> 
<div id="title">
  <h1 class="inner title-2">Search Results <span>- Web Developer</span>
    <ul class="breadcrumb-inner">
      <li> <a href="index.php">Home</a></li>
      <li> <a href="candidates.php">Candidates</a></li>
    </ul>
  </h1>
</div>  
  <div class="inner"> 
    
    <!-- Content Inner -->
    <div class="content-inner"> 
      
      <!-- Content Center -->
      <div class="content-center candidate-list">
      <div id="search-and-sort" class="box-1 search-bar-half">
      <div id="search-partner">
        <form id="search-partner-form" action="post">
          <input type="text" placeholder="Location" class="textfield-with-callback"/>
          <input type="text" placeholder="Industry / Role" class="textfield-with-callback"/>
          <input id="search-submit" class="pull-right" type="submit" value="Search">
        </form>
      </div>
    </div>
        
        <div class="clear"></div>
        <div class="heading-l">
          <h2> Available Jobs </h2>
        </div>
        
       <div class="job-page-top-nav-bar">

                    <div class="job-page-sorter">
                        <div class="sorter-select">
                            <select class="select">
                                <option value="Sort By" selected="selected">- Sort By -</option>
                                <option value="Sort Criterion 1">Sort Criterion 1</option>
                                <option value="Sort Criterion 2">Sort Criterion 2</option>
                                <option value="Sort Criterion 3">Sort Criterion 3</option>
                            </select>
                        </div>
                    </div>
                     <div class="search-count">451 Results Found</div>
                    <div class="pager">
                        <ul>
                            <li class="prev noactive"><a></a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">6</a></li>
                            <li class="next"><a href="#"></a></li>
                        </ul>
                    </div>

                </div>
          
          <div id="job-content-fields">
        
          <div id="cells" class="view_mode">
            <div  class="field-container odd box-1">
              <div class="nav-buttons">
                <ul>
                  <li class="show-hide"><a></a></li>
                  <li class="favorite"><a href="#"></a></li>
                  <li class="link"><a href="job.php"></a></li>
                </ul>
              </div>
              <div class="cells-job-thumb"> <img src="images/recruiter.jpg"  alt="Thumb"/> </div>
              <div class="header-fields">
                <div class="title-company">
                  <div class="title"><a href="job.php">Abu Antar</a></div>
                  <div class="company">24 Years Old - Sydney, AU</div>
                </div>
                <ul class="social_media_icons job">
                  <li> <a href="#"> <i class="fa fa-facebook"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-google-plus"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-twitter"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-linkedin-square"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-pinterest"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-dribbble"></i> </a> </li>
                </ul>
              </div>
              <div class="body-field">
                <div class="teaser">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Lorem ipsum dolor sit amet, consectetur adipisicing elit.<span class="read-more"><a>Read More</a></span></p>
                </div>
                <div class="full-body">
                  <p>Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
                <ul class="candidate-meta meta-fields">
                  <li class="pull-left">Experience: <span>5 Years</span></li>
                  <li class="pull-center">Degree: <span>MBA</span></li>
                  <li class="pull-right">Career Level: <span>Mid Career</span></li>
                </ul>
                <div class="block-fields">
                  <div class="block skills">
                    <h4>Required Skills</h4>
                    <div class="block-content">
                      <div class = "field roll-with-description hide">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">5 Years of Experience</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "60"></span></div>
                          <div class = "description">...</div>
                        </div>
                      </div>
                      <div class = "field roll-with-description hide">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">Perfect Written &amp; Spoken English</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "100"></span></div>
                          <div class = "description">...</div>
                        </div>
                      </div>
                      <div class = "field roll-with-description show">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">3 Foreign Languages</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "65"></span></div>
                          <div class = "description">Preferred languages are Arabic, French &amp; Italian. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi.</div>
                        </div>
                      </div>
                      <!-- Cleaner -->
                      <div class="clear"></div>
                      <!-- /Cleaner --> 
                    </div>
                  </div>
                  <div class="block">
                    <h4>Additional Requirements</h4>
                    <div class="block-content">
                      <div class = "tag-field">Work Permit</div>
                      <div class = "tag-field">5 Years Experience</div>
                      <div class = "tag-field">MBA</div>
                      <div class = "tag-field">Magento Certified</div>
                      <div class = "tag-field">Perfect Written &amp; Spoken English</div>
                    </div>
                    <!-- Cleaner -->
                    <div class="clear"></div>
                    <!-- /Cleaner --> 
                  </div>
                </div>
                <div class="buttons-field applybtns">
                  <div class="apply"><a href="#">Contact Me</a></div>
                  <div class="full"><a href="#">Contact On MotibU</a></div>
                </div>
              </div>
            </div>
             <div  class="field-container odd box-1">
              <div class="nav-buttons">
                <ul>
                  <li class="show-hide"><a></a></li>
                  <li class="favorite"><a href="#"></a></li>
                  <li class="link"><a href="job.php"></a></li>
                </ul>
              </div>
              <div class="cells-job-thumb"> <img src="images/recruiter.jpg"  alt="Thumb"/> </div>
              <div class="header-fields">
                <div class="title-company">
                  <div class="title"><a href="job.php">Abu Antar</a></div>
                  <div class="company">24 Years Old - Sydney, AU</div>
                </div>
                <ul class="social_media_icons job">
                  <li> <a href="#"> <i class="fa fa-facebook"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-google-plus"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-twitter"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-linkedin-square"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-pinterest"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-dribbble"></i> </a> </li>
                </ul>
              </div>
              <div class="body-field">
                <div class="teaser">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Lorem ipsum dolor sit amet, consectetur adipisicing elit.<span class="read-more"><a>Read More</a></span></p>
                </div>
                <div class="full-body">
                  <p>Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
                <ul class="candidate-meta meta-fields">
                  <li class="pull-left">Experience: <span>5 Years</span></li>
                  <li class="pull-center">Degree: <span>MBA</span></li>
                  <li class="pull-right">Career Level: <span>Mid Career</span></li>
                </ul>
                <div class="block-fields">
                  <div class="block skills">
                    <h4>Required Skills</h4>
                    <div class="block-content">
                      <div class = "field roll-with-description hide">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">5 Years of Experience</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "60"></span></div>
                          <div class = "description">...</div>
                        </div>
                      </div>
                      <div class = "field roll-with-description hide">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">Perfect Written &amp; Spoken English</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "100"></span></div>
                          <div class = "description">...</div>
                        </div>
                      </div>
                      <div class = "field roll-with-description show">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">3 Foreign Languages</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "65"></span></div>
                          <div class = "description">Preferred languages are Arabic, French &amp; Italian. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi.</div>
                        </div>
                      </div>
                      <!-- Cleaner -->
                      <div class="clear"></div>
                      <!-- /Cleaner --> 
                    </div>
                  </div>
                  <div class="block">
                    <h4>Additional Requirements</h4>
                    <div class="block-content">
                      <div class = "tag-field">Work Permit</div>
                      <div class = "tag-field">5 Years Experience</div>
                      <div class = "tag-field">MBA</div>
                      <div class = "tag-field">Magento Certified</div>
                      <div class = "tag-field">Perfect Written &amp; Spoken English</div>
                    </div>
                    <!-- Cleaner -->
                    <div class="clear"></div>
                    <!-- /Cleaner --> 
                  </div>
                </div>
                <div class="buttons-field applybtns">
                  <div class="apply"><a href="#">Contact Me</a></div>
                  <div class="full"><a href="#">Contact On MotibU</a></div>
                </div>
              </div>
            </div>
             <div  class="field-container odd box-1">
              <div class="nav-buttons">
                <ul>
                  <li class="show-hide"><a></a></li>
                  <li class="favorite"><a href="#"></a></li>
                  <li class="link"><a href="job.php"></a></li>
                </ul>
              </div>
              <div class="cells-job-thumb"> <img src="images/recruiter.jpg"  alt="Thumb"/> </div>
              <div class="header-fields">
                <div class="title-company">
                  <div class="title"><a href="job.php">Abu Antar</a></div>
                  <div class="company">24 Years Old - Sydney, AU</div>
                </div>
                <ul class="social_media_icons job">
                  <li> <a href="#"> <i class="fa fa-facebook"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-google-plus"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-twitter"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-linkedin-square"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-pinterest"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-dribbble"></i> </a> </li>
                </ul>
              </div>
              <div class="body-field">
                <div class="teaser">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Lorem ipsum dolor sit amet, consectetur adipisicing elit.<span class="read-more"><a>Read More</a></span></p>
                </div>
                <div class="full-body">
                  <p>Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
                <ul class="candidate-meta meta-fields">
                  <li class="pull-left">Experience: <span>5 Years</span></li>
                  <li class="pull-center">Degree: <span>MBA</span></li>
                  <li class="pull-right">Career Level: <span>Mid Career</span></li>
                </ul>
                <div class="block-fields">
                  <div class="block skills">
                    <h4>Required Skills</h4>
                    <div class="block-content">
                      <div class = "field roll-with-description hide">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">5 Years of Experience</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "60"></span></div>
                          <div class = "description">...</div>
                        </div>
                      </div>
                      <div class = "field roll-with-description hide">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">Perfect Written &amp; Spoken English</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "100"></span></div>
                          <div class = "description">...</div>
                        </div>
                      </div>
                      <div class = "field roll-with-description show">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">3 Foreign Languages</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "65"></span></div>
                          <div class = "description">Preferred languages are Arabic, French &amp; Italian. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi.</div>
                        </div>
                      </div>
                      <!-- Cleaner -->
                      <div class="clear"></div>
                      <!-- /Cleaner --> 
                    </div>
                  </div>
                  <div class="block">
                    <h4>Additional Requirements</h4>
                    <div class="block-content">
                      <div class = "tag-field">Work Permit</div>
                      <div class = "tag-field">5 Years Experience</div>
                      <div class = "tag-field">MBA</div>
                      <div class = "tag-field">Magento Certified</div>
                      <div class = "tag-field">Perfect Written &amp; Spoken English</div>
                    </div>
                    <!-- Cleaner -->
                    <div class="clear"></div>
                    <!-- /Cleaner --> 
                  </div>
                </div>
                <div class="buttons-field applybtns">
                  <div class="apply"><a href="#">Contact Me</a></div>
                  <div class="full"><a href="#">Contact On MotibU</a></div>
                </div>
              </div>
            </div>
             <div  class="field-container odd box-1">
              <div class="nav-buttons">
                <ul>
                  <li class="show-hide"><a></a></li>
                  <li class="favorite"><a href="#"></a></li>
                  <li class="link"><a href="job.php"></a></li>
                </ul>
              </div>
              <div class="cells-job-thumb"> <img src="images/recruiter.jpg"  alt="Thumb"/> </div>
              <div class="header-fields">
                <div class="title-company">
                  <div class="title"><a href="job.php">Abu Antar</a></div>
                  <div class="company">24 Years Old - Sydney, AU</div>
                </div>
                <ul class="social_media_icons job">
                  <li> <a href="#"> <i class="fa fa-facebook"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-google-plus"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-twitter"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-linkedin-square"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-pinterest"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-dribbble"></i> </a> </li>
                </ul>
              </div>
              <div class="body-field">
                <div class="teaser">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Lorem ipsum dolor sit amet, consectetur adipisicing elit.<span class="read-more"><a>Read More</a></span></p>
                </div>
                <div class="full-body">
                  <p>Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
                <ul class="candidate-meta meta-fields">
                  <li class="pull-left">Experience: <span>5 Years</span></li>
                  <li class="pull-center">Degree: <span>MBA</span></li>
                  <li class="pull-right">Career Level: <span>Mid Career</span></li>
                </ul>
                <div class="block-fields">
                  <div class="block skills">
                    <h4>Required Skills</h4>
                    <div class="block-content">
                      <div class = "field roll-with-description hide">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">5 Years of Experience</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "60"></span></div>
                          <div class = "description">...</div>
                        </div>
                      </div>
                      <div class = "field roll-with-description hide">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">Perfect Written &amp; Spoken English</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "100"></span></div>
                          <div class = "description">...</div>
                        </div>
                      </div>
                      <div class = "field roll-with-description show">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">3 Foreign Languages</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "65"></span></div>
                          <div class = "description">Preferred languages are Arabic, French &amp; Italian. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi.</div>
                        </div>
                      </div>
                      <!-- Cleaner -->
                      <div class="clear"></div>
                      <!-- /Cleaner --> 
                    </div>
                  </div>
                  <div class="block">
                    <h4>Additional Requirements</h4>
                    <div class="block-content">
                      <div class = "tag-field">Work Permit</div>
                      <div class = "tag-field">5 Years Experience</div>
                      <div class = "tag-field">MBA</div>
                      <div class = "tag-field">Magento Certified</div>
                      <div class = "tag-field">Perfect Written &amp; Spoken English</div>
                    </div>
                    <!-- Cleaner -->
                    <div class="clear"></div>
                    <!-- /Cleaner --> 
                  </div>
                </div>
                <div class="buttons-field applybtns">
                  <div class="apply"><a href="#">Contact Me</a></div>
                  <div class="full"><a href="#">Contact On MotibU</a></div>
                </div>
              </div>
            </div>
             <div  class="field-container odd box-1">
              <div class="nav-buttons">
                <ul>
                  <li class="show-hide"><a></a></li>
                  <li class="favorite"><a href="#"></a></li>
                  <li class="link"><a href="job.php"></a></li>
                </ul>
              </div>
              <div class="cells-job-thumb"> <img src="images/recruiter.jpg"  alt="Thumb"/> </div>
              <div class="header-fields">
                <div class="title-company">
                  <div class="title"><a href="job.php">Abu Antar</a></div>
                  <div class="company">24 Years Old - Sydney, AU</div>
                </div>
                <ul class="social_media_icons job">
                  <li> <a href="#"> <i class="fa fa-facebook"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-google-plus"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-twitter"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-linkedin-square"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-pinterest"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-dribbble"></i> </a> </li>
                </ul>
              </div>
              <div class="body-field">
                <div class="teaser">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Lorem ipsum dolor sit amet, consectetur adipisicing elit.<span class="read-more"><a>Read More</a></span></p>
                </div>
                <div class="full-body">
                  <p>Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
                <ul class="candidate-meta meta-fields">
                  <li class="pull-left">Experience: <span>5 Years</span></li>
                  <li class="pull-center">Degree: <span>MBA</span></li>
                  <li class="pull-right">Career Level: <span>Mid Career</span></li>
                </ul>
                <div class="block-fields">
                  <div class="block skills">
                    <h4>Required Skills</h4>
                    <div class="block-content">
                      <div class = "field roll-with-description hide">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">5 Years of Experience</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "60"></span></div>
                          <div class = "description">...</div>
                        </div>
                      </div>
                      <div class = "field roll-with-description hide">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">Perfect Written &amp; Spoken English</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "100"></span></div>
                          <div class = "description">...</div>
                        </div>
                      </div>
                      <div class = "field roll-with-description show">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">3 Foreign Languages</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "65"></span></div>
                          <div class = "description">Preferred languages are Arabic, French &amp; Italian. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi.</div>
                        </div>
                      </div>
                      <!-- Cleaner -->
                      <div class="clear"></div>
                      <!-- /Cleaner --> 
                    </div>
                  </div>
                  <div class="block">
                    <h4>Additional Requirements</h4>
                    <div class="block-content">
                      <div class = "tag-field">Work Permit</div>
                      <div class = "tag-field">5 Years Experience</div>
                      <div class = "tag-field">MBA</div>
                      <div class = "tag-field">Magento Certified</div>
                      <div class = "tag-field">Perfect Written &amp; Spoken English</div>
                    </div>
                    <!-- Cleaner -->
                    <div class="clear"></div>
                    <!-- /Cleaner --> 
                  </div>
                </div>
                <div class="buttons-field applybtns">
                  <div class="apply"><a href="#">Contact Me</a></div>
                  <div class="full"><a href="#">Contact On MotibU</a></div>
                </div>
              </div>
            </div>
             <div  class="field-container odd box-1">
              <div class="nav-buttons">
                <ul>
                  <li class="show-hide"><a></a></li>
                  <li class="favorite"><a href="#"></a></li>
                  <li class="link"><a href="job.php"></a></li>
                </ul>
              </div>
              <div class="cells-job-thumb"> <img src="images/recruiter.jpg"  alt="Thumb"/> </div>
              <div class="header-fields">
                <div class="title-company">
                  <div class="title"><a href="job.php">Abu Antar</a></div>
                  <div class="company">24 Years Old - Sydney, AU</div>
                </div>
                <ul class="social_media_icons job">
                  <li> <a href="#"> <i class="fa fa-facebook"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-google-plus"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-twitter"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-linkedin-square"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-pinterest"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-dribbble"></i> </a> </li>
                </ul>
              </div>
              <div class="body-field">
                <div class="teaser">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Lorem ipsum dolor sit amet, consectetur adipisicing elit.<span class="read-more"><a>Read More</a></span></p>
                </div>
                <div class="full-body">
                  <p>Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
                <ul class="candidate-meta meta-fields">
                  <li class="pull-left">Experience: <span>5 Years</span></li>
                  <li class="pull-center">Degree: <span>MBA</span></li>
                  <li class="pull-right">Career Level: <span>Mid Career</span></li>
                </ul>
                <div class="block-fields">
                  <div class="block skills">
                    <h4>Required Skills</h4>
                    <div class="block-content">
                      <div class = "field roll-with-description hide">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">5 Years of Experience</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "60"></span></div>
                          <div class = "description">...</div>
                        </div>
                      </div>
                      <div class = "field roll-with-description hide">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">Perfect Written &amp; Spoken English</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "100"></span></div>
                          <div class = "description">...</div>
                        </div>
                      </div>
                      <div class = "field roll-with-description show">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">3 Foreign Languages</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "65"></span></div>
                          <div class = "description">Preferred languages are Arabic, French &amp; Italian. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi.</div>
                        </div>
                      </div>
                      <!-- Cleaner -->
                      <div class="clear"></div>
                      <!-- /Cleaner --> 
                    </div>
                  </div>
                  <div class="block">
                    <h4>Additional Requirements</h4>
                    <div class="block-content">
                      <div class = "tag-field">Work Permit</div>
                      <div class = "tag-field">5 Years Experience</div>
                      <div class = "tag-field">MBA</div>
                      <div class = "tag-field">Magento Certified</div>
                      <div class = "tag-field">Perfect Written &amp; Spoken English</div>
                    </div>
                    <!-- Cleaner -->
                    <div class="clear"></div>
                    <!-- /Cleaner --> 
                  </div>
                </div>
                <div class="buttons-field applybtns">
                  <div class="apply"><a href="#">Contact Me</a></div>
                  <div class="full"><a href="#">Contact On MotibU</a></div>
                </div>
              </div>
            </div>
             <div  class="field-container odd box-1">
              <div class="nav-buttons">
                <ul>
                  <li class="show-hide"><a></a></li>
                  <li class="favorite"><a href="#"></a></li>
                  <li class="link"><a href="job.php"></a></li>
                </ul>
              </div>
              <div class="cells-job-thumb"> <img src="images/recruiter.jpg"  alt="Thumb"/> </div>
              <div class="header-fields">
                <div class="title-company">
                  <div class="title"><a href="job.php">Abu Antar</a></div>
                  <div class="company">24 Years Old - Sydney, AU</div>
                </div>
                <ul class="social_media_icons job">
                  <li> <a href="#"> <i class="fa fa-facebook"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-google-plus"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-twitter"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-linkedin-square"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-pinterest"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-dribbble"></i> </a> </li>
                </ul>
              </div>
              <div class="body-field">
                <div class="teaser">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Lorem ipsum dolor sit amet, consectetur adipisicing elit.<span class="read-more"><a>Read More</a></span></p>
                </div>
                <div class="full-body">
                  <p>Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
                <ul class="candidate-meta meta-fields">
                  <li class="pull-left">Experience: <span>5 Years</span></li>
                  <li class="pull-center">Degree: <span>MBA</span></li>
                  <li class="pull-right">Career Level: <span>Mid Career</span></li>
                </ul>
                <div class="block-fields">
                  <div class="block skills">
                    <h4>Required Skills</h4>
                    <div class="block-content">
                      <div class = "field roll-with-description hide">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">5 Years of Experience</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "60"></span></div>
                          <div class = "description">...</div>
                        </div>
                      </div>
                      <div class = "field roll-with-description hide">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">Perfect Written &amp; Spoken English</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "100"></span></div>
                          <div class = "description">...</div>
                        </div>
                      </div>
                      <div class = "field roll-with-description show">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">3 Foreign Languages</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "65"></span></div>
                          <div class = "description">Preferred languages are Arabic, French &amp; Italian. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi.</div>
                        </div>
                      </div>
                      <!-- Cleaner -->
                      <div class="clear"></div>
                      <!-- /Cleaner --> 
                    </div>
                  </div>
                  <div class="block">
                    <h4>Additional Requirements</h4>
                    <div class="block-content">
                      <div class = "tag-field">Work Permit</div>
                      <div class = "tag-field">5 Years Experience</div>
                      <div class = "tag-field">MBA</div>
                      <div class = "tag-field">Magento Certified</div>
                      <div class = "tag-field">Perfect Written &amp; Spoken English</div>
                    </div>
                    <!-- Cleaner -->
                    <div class="clear"></div>
                    <!-- /Cleaner --> 
                  </div>
                </div>
                <div class="buttons-field applybtns">
                  <div class="apply"><a href="#">Contact Me</a></div>
                  <div class="full"><a href="#">Contact On MotibU</a></div>
                </div>
              </div>
            </div>
             <div  class="field-container odd box-1">
              <div class="nav-buttons">
                <ul>
                  <li class="show-hide"><a></a></li>
                  <li class="favorite"><a href="#"></a></li>
                  <li class="link"><a href="job.php"></a></li>
                </ul>
              </div>
              <div class="cells-job-thumb"> <img src="images/recruiter.jpg"  alt="Thumb"/> </div>
              <div class="header-fields">
                <div class="title-company">
                  <div class="title"><a href="job.php">Abu Antar</a></div>
                  <div class="company">24 Years Old - Sydney, AU</div>
                </div>
                <ul class="social_media_icons job">
                  <li> <a href="#"> <i class="fa fa-facebook"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-google-plus"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-twitter"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-linkedin-square"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-pinterest"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-dribbble"></i> </a> </li>
                </ul>
              </div>
              <div class="body-field">
                <div class="teaser">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Lorem ipsum dolor sit amet, consectetur adipisicing elit.<span class="read-more"><a>Read More</a></span></p>
                </div>
                <div class="full-body">
                  <p>Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
                <ul class="candidate-meta meta-fields">
                  <li class="pull-left">Experience: <span>5 Years</span></li>
                  <li class="pull-center">Degree: <span>MBA</span></li>
                  <li class="pull-right">Career Level: <span>Mid Career</span></li>
                </ul>
                <div class="block-fields">
                  <div class="block skills">
                    <h4>Required Skills</h4>
                    <div class="block-content">
                      <div class = "field roll-with-description hide">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">5 Years of Experience</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "60"></span></div>
                          <div class = "description">...</div>
                        </div>
                      </div>
                      <div class = "field roll-with-description hide">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">Perfect Written &amp; Spoken English</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "100"></span></div>
                          <div class = "description">...</div>
                        </div>
                      </div>
                      <div class = "field roll-with-description show">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">3 Foreign Languages</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "65"></span></div>
                          <div class = "description">Preferred languages are Arabic, French &amp; Italian. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi.</div>
                        </div>
                      </div>
                      <!-- Cleaner -->
                      <div class="clear"></div>
                      <!-- /Cleaner --> 
                    </div>
                  </div>
                  <div class="block">
                    <h4>Additional Requirements</h4>
                    <div class="block-content">
                      <div class = "tag-field">Work Permit</div>
                      <div class = "tag-field">5 Years Experience</div>
                      <div class = "tag-field">MBA</div>
                      <div class = "tag-field">Magento Certified</div>
                      <div class = "tag-field">Perfect Written &amp; Spoken English</div>
                    </div>
                    <!-- Cleaner -->
                    <div class="clear"></div>
                    <!-- /Cleaner --> 
                  </div>
                </div>
                <div class="buttons-field applybtns">
                  <div class="apply"><a href="#">Contact Me</a></div>
                  <div class="full"><a href="#">Contact On MotibU</a></div>
                </div>
              </div>
            </div>
             <div  class="field-container odd box-1">
              <div class="nav-buttons">
                <ul>
                  <li class="show-hide"><a></a></li>
                  <li class="favorite"><a href="#"></a></li>
                  <li class="link"><a href="job.php"></a></li>
                </ul>
              </div>
              <div class="cells-job-thumb"> <img src="images/recruiter.jpg"  alt="Thumb"/> </div>
              <div class="header-fields">
                <div class="title-company">
                  <div class="title"><a href="job.php">Abu Antar</a></div>
                  <div class="company">24 Years Old - Sydney, AU</div>
                </div>
                <ul class="social_media_icons job">
                  <li> <a href="#"> <i class="fa fa-facebook"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-google-plus"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-twitter"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-linkedin-square"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-pinterest"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-dribbble"></i> </a> </li>
                </ul>
              </div>
              <div class="body-field">
                <div class="teaser">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Lorem ipsum dolor sit amet, consectetur adipisicing elit.<span class="read-more"><a>Read More</a></span></p>
                </div>
                <div class="full-body">
                  <p>Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
                <ul class="candidate-meta meta-fields">
                  <li class="pull-left">Experience: <span>5 Years</span></li>
                  <li class="pull-center">Degree: <span>MBA</span></li>
                  <li class="pull-right">Career Level: <span>Mid Career</span></li>
                </ul>
                <div class="block-fields">
                  <div class="block skills">
                    <h4>Required Skills</h4>
                    <div class="block-content">
                      <div class = "field roll-with-description hide">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">5 Years of Experience</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "60"></span></div>
                          <div class = "description">...</div>
                        </div>
                      </div>
                      <div class = "field roll-with-description hide">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">Perfect Written &amp; Spoken English</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "100"></span></div>
                          <div class = "description">...</div>
                        </div>
                      </div>
                      <div class = "field roll-with-description show">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">3 Foreign Languages</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "65"></span></div>
                          <div class = "description">Preferred languages are Arabic, French &amp; Italian. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi.</div>
                        </div>
                      </div>
                      <!-- Cleaner -->
                      <div class="clear"></div>
                      <!-- /Cleaner --> 
                    </div>
                  </div>
                  <div class="block">
                    <h4>Additional Requirements</h4>
                    <div class="block-content">
                      <div class = "tag-field">Work Permit</div>
                      <div class = "tag-field">5 Years Experience</div>
                      <div class = "tag-field">MBA</div>
                      <div class = "tag-field">Magento Certified</div>
                      <div class = "tag-field">Perfect Written &amp; Spoken English</div>
                    </div>
                    <!-- Cleaner -->
                    <div class="clear"></div>
                    <!-- /Cleaner --> 
                  </div>
                </div>
                <div class="buttons-field applybtns">
                  <div class="apply"><a href="#">Contact Me</a></div>
                  <div class="full"><a href="#">Contact On MotibU</a></div>
                </div>
              </div>
            </div>
             <div  class="field-container odd box-1">
              <div class="nav-buttons">
                <ul>
                  <li class="show-hide"><a></a></li>
                  <li class="favorite"><a href="#"></a></li>
                  <li class="link"><a href="job.php"></a></li>
                </ul>
              </div>
              <div class="cells-job-thumb"> <img src="images/recruiter.jpg"  alt="Thumb"/> </div>
              <div class="header-fields">
                <div class="title-company">
                  <div class="title"><a href="job.php">Abu Antar</a></div>
                  <div class="company">24 Years Old - Sydney, AU</div>
                </div>
                <ul class="social_media_icons job">
                  <li> <a href="#"> <i class="fa fa-facebook"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-google-plus"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-twitter"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-linkedin-square"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-pinterest"></i> </a> </li>
                  <li> <a href="#"> <i class="fa fa-dribbble"></i> </a> </li>
                </ul>
              </div>
              <div class="body-field">
                <div class="teaser">
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Lorem ipsum dolor sit amet, consectetur adipisicing elit.<span class="read-more"><a>Read More</a></span></p>
                </div>
                <div class="full-body">
                  <p>Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                </div>
                <ul class="candidate-meta meta-fields">
                  <li class="pull-left">Experience: <span>5 Years</span></li>
                  <li class="pull-center">Degree: <span>MBA</span></li>
                  <li class="pull-right">Career Level: <span>Mid Career</span></li>
                </ul>
                <div class="block-fields">
                  <div class="block skills">
                    <h4>Required Skills</h4>
                    <div class="block-content">
                      <div class = "field roll-with-description hide">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">5 Years of Experience</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "60"></span></div>
                          <div class = "description">...</div>
                        </div>
                      </div>
                      <div class = "field roll-with-description hide">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">Perfect Written &amp; Spoken English</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "100"></span></div>
                          <div class = "description">...</div>
                        </div>
                      </div>
                      <div class = "field roll-with-description show">
                        <div class = "roll-button"><span></span></div>
                        <div class = "roll-field">
                          <div class = "label">3 Foreign Languages</div>
                          <div class = "progressbar"><span class = "progress-count" data-level = "65"></span></div>
                          <div class = "description">Preferred languages are Arabic, French &amp; Italian. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi.</div>
                        </div>
                      </div>
                      <!-- Cleaner -->
                      <div class="clear"></div>
                      <!-- /Cleaner --> 
                    </div>
                  </div>
                  <div class="block">
                    <h4>Additional Requirements</h4>
                    <div class="block-content">
                      <div class = "tag-field">Work Permit</div>
                      <div class = "tag-field">5 Years Experience</div>
                      <div class = "tag-field">MBA</div>
                      <div class = "tag-field">Magento Certified</div>
                      <div class = "tag-field">Perfect Written &amp; Spoken English</div>
                    </div>
                    <!-- Cleaner -->
                    <div class="clear"></div>
                    <!-- /Cleaner --> 
                  </div>
                </div>
                <div class="buttons-field applybtns">
                  <div class="apply"><a href="#">Contact Me</a></div>
                  <div class="full"><a href="#">Contact On MotibU</a></div>
                </div>
              </div>
            </div>
          
          </div>
        
        </div> 
      
      <div class="clearfix"></div>
      <div class="job-page-top-nav-bar">

                    <div class="job-page-sorter">
                        <div class="sorter-select">
                            <select class="select">
                                <option value="Sort By" selected="selected">- Sort By -</option>
                                <option value="Sort Criterion 1">Sort Criterion 1</option>
                                <option value="Sort Criterion 2">Sort Criterion 2</option>
                                <option value="Sort Criterion 3">Sort Criterion 3</option>
                            </select>
                        </div>
                    </div>

                    <div class="pager">
                        <ul>
                            <li class="prev noactive"><a></a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">6</a></li>
                            <li class="next"><a href="#"></a></li>
                        </ul>
                    </div>

                </div>
      </div>
      <!-- /Content Center --> 
      
      <!-- Content Right -->
      <div class="content-right">
        <div id="social-like" class="block background">
          <h2 class="title-1">Share Us With Your Friends</h2>
          <div class="block-content">
            <div class="social-like linkedin"><a class="like-button" href="#">LinkedIn</a><span class="count">500</span></div>
            <div class="social-like motibu"><a class="like-button" href="#">Motibu</a><span class="count">2.7K</span></div>
            <div class="social-like facebook"><a class="like-button" href="#">Facebook</a><span class="count">1.3K</span></div>
            <div class="social-like twitter"><a class="like-button" href="#">Twitter</a><span class="count">868</span></div>
          </div>
        </div>
        <div id="advertising" class="block border">
          <div class="block-content"> <img src="images/sb-ad.jpg"  alt="sidebarad"/> 
            <!-- <div class="advertising-test">300x250<br/>
              Ad Banner</div>--> 
          </div>
        </div>
        <div id="poll" class="block background">
          <h2 class="title-1">5 Tips to pass your interview!</h2>
          <!--<iframe width="260" height="150" src="http://www.youtube.com/embed/wqGfDdJDONI?wmode=transparent" frameborder="0" allowfullscreen></iframe>-->
          <div class="ad-wrapper"> <img src="images/sb-video.jpg"  alt="sb-video"/> </div>
          <h2 class="title-1">The Poll</h2>
          <div class="block-content">
            <form id="poll-form" action="post">
              <ul id="radio-checkbox-list">
                <li>
                  <input id="poll_choice_1" type="radio" name="poll-choice" value="1" />
                  <label for="poll_choice_1" >Definitely Yes</label>
                </li>
                <li>
                  <input id="poll_choice_2" type="radio" name="poll-choice" value="2"/>
                  <label for="poll_choice_2">Rather Yes</label>
                </li>
                <li>
                  <input id="poll_choice_3" type="radio" name="poll-choice" value="3"/>
                  <label for="poll_choice_3">I’m Not Sure</label>
                </li>
                <li>
                  <input id="poll_choice_4" type="radio" name="poll-choice" value="4"/>
                  <label for="poll_choice_4">Rather Not</label>
                </li>
                <li>
                  <input id="poll_choice_5" type="radio" name="poll-choice" value="5" />
                  <label for="poll_choice_5">No Way!</label>
                </li>
              </ul>
              <button class="btn blue">Vote Now</button>
            </form>
          </div>
        </div>
        <div id="google" class="block">
          <div class="block_title"> Ads by Google </div>
          <div class="block_content">
            <div class="one_ad">
              <div class="title"> Find Your Dream Job With Career </div>
              <div class="text"> Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus. </div>
              <div class="link"><a href="#">example.com</a></div>
            </div>
            <div class="one_ad">
              <div class="title"> Find Your Dream Job With Career </div>
              <div class="text"> Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus. </div>
              <div class="link"><a href="#">example.com</a></div>
            </div>
            <div class="one_ad">
              <div class="title"> Find Your Dream Job With Career </div>
              <div class="text"> Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus. </div>
              <div class="link"><a href="#">example.com</a></div>
            </div>
            <div class="one_ad">
              <div class="title"> Find Your Dream Job With Career </div>
              <div class="text"> Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus. </div>
              <div class="link"><a href="#">example.com</a></div>
            </div>
            <div class="one_ad">
              <div class="title"> Find Your Dream Job With Career </div>
              <div class="text"> Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus. </div>
              <div class="link"><a href="#">example.com</a></div>
            </div>
          </div>
        </div>
      </div>
      <!-- /Content Right -->
      
      <div class="clear"></div>
      
      <!-- Clear Line --> 
      
    </div>
    <!-- /Content Inner --> 
    
  </div>
</div>
<!-- /Content --> 

<!-- Footer -->
<?php include('footer.php');?>

</body>
</html>