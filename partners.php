<!DOCTYPE html>
<html class="no-js pattern_1">
<head>
<title>Partners</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Lato:300,400,700&amp;subset=latin,latin-ext"/>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-responsive.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/reset.css"/>
<link id="color_css" rel="stylesheet" type="text/css" href="css/color_scheme_1.css"/>
<link rel="stylesheet" type="text/css" href="css/jquery.combosex.css"/>
<link rel="stylesheet" type="text/css" href="css/jquery.flexslider.css"/>
<link rel="stylesheet" type="text/css" href="css/jquery.scrollbar.css"/>

<!--[if (lte IE 9)]>
    <link rel="stylesheet" type="text/css" href="css/iefix.css"/>
    <![endif]-->
<script type="text/javascript" src="js/jquery.1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery.combosex.min.js"></script>
<script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="js/jquery.easytabs.min.js"></script>
<script type="text/javascript" src="js/jquery.gmap.min.js"></script>
<script type="text/javascript" src="js/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
</head>
<body>

<?php include ('topheader.php'); ?>
<?php include ('header.php'); ?>

<!-- Content -->
<div id="content">
<div id="title">
  <h1 class="inner title-2">Partners
    <ul class="breadcrumb-inner">
      <li> <a href="index.php">Home</a></li>
      <li> <a href="partners.php">Partners</a></li>
    </ul>
  </h1>
</div>
<div class="inner">
  <div class="content-inner">
  
    <div id="search-and-sort" class="box-1 search-bar-partner">
      <div id="search-partner">
        <form id="search-partner-form" action="post">
          <input type="text" placeholder="Search for Partners" class="textfield-with-callback"/>
          <div id="sort-partner" class="sort-Industry">
            <select class="select">
              <option selected="selected" value="nothing">- Select Industry-</option>
              <option value="sorting criteria 1">Sorting Criteria 1</option>
              <option value="sorting criteria 2">Sorting Criteria 2</option>
              <option value="sorting criteria 3">Sorting Criteria 3</option>
              <option value="sorting criteria 4">Sorting Criteria 4</option>
              <option value="sorting criteria 5">Sorting Criteria 5</option>
            </select>
          </div>
          <input id="search-submit" type="submit" value="Search">
          
           <div id="page-partners">
        <select class="select">
          <option selected="selected" value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
        </select>
      </div>
      <div id="sort-partner">
        <select class="select">
          <option selected="selected" value="nothing">- Sort By -</option>
          <option value="sorting criteria 1">Sorting Criteria 1</option>
          <option value="sorting criteria 2">Sorting Criteria 2</option>
          <option value="sorting criteria 3">Sorting Criteria 3</option>
          <option value="sorting criteria 4">Sorting Criteria 4</option>
          <option value="sorting criteria 5">Sorting Criteria 5</option>
        </select>
      </div>
        </form>
      </div>
     
    </div>
    
    <!-- Content Inner -->
    <div class="content-inner">
      <div id="our-partners">
        <div class="partner">
          <div class="partner-thumb"> <a href="#"><img class="part-img" src="images/partner-1.jpg"  alt="Partner"/></a>
            <div class="partner-hover">
              <h4>Audiojungle</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a.</p>
              <p><strong>Industry:</strong> Audio, Sound, Music</p>
            </div>
          </div>
          <div class="partners-meta">
            <div class="partner-jobs"> Jobs: 200 </div>
            <div class="nav-buttons">
              <ul>
                <li class="search"><a><img src="images/p-zoom.png"  alt=""/></a></li>
                <li class="link"><a href="job.php"><img src="images/p-zoom-02.png"  alt=""/></a></li>
              </ul>
            </div>
          </div>
        </div>
      <div class="partner">
          <div class="partner-thumb"> <a href="#"><img class="part-img" src="images/partner-2.jpg"  alt="Partner"/></a>
            <div class="partner-hover">
              <h4>Audiojungle</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a.</p>
              <p><strong>Industry:</strong> Audio, Sound, Music</p>
            </div>
          </div>
          <div class="partners-meta">
            <div class="partner-jobs"> Jobs: 200 </div>
            <div class="nav-buttons">
              <ul>
                <li class="search"><a><img src="images/p-zoom.png"  alt=""/></a></li>
                <li class="link"><a href="job.php"><img src="images/p-zoom-02.png"  alt=""/></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="partner">
          <div class="partner-thumb"> <a href="#"><img class="part-img" src="images/partner-3.jpg"  alt="Partner"/></a>
            <div class="partner-hover">
              <h4>Audiojungle</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a.</p>
              <p><strong>Industry:</strong> Audio, Sound, Music</p>
            </div>
          </div>
          <div class="partners-meta">
            <div class="partner-jobs"> Jobs: 200 </div>
            <div class="nav-buttons">
              <ul>
                <li class="search"><a><img src="images/p-zoom.png"  alt=""/></a></li>
                <li class="link"><a href="job.php"><img src="images/p-zoom-02.png"  alt=""/></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="partner">
          <div class="partner-thumb"> <a href="#"><img class="part-img" src="images/partner-4.jpg"  alt="Partner"/></a>
            <div class="partner-hover">
              <h4>Audiojungle</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a.</p>
              <p><strong>Industry:</strong> Audio, Sound, Music</p>
            </div>
          </div>
          <div class="partners-meta">
            <div class="partner-jobs"> Jobs: 200 </div>
            <div class="nav-buttons">
              <ul>
                <li class="search"><a><img src="images/p-zoom.png"  alt=""/></a></li>
                <li class="link"><a href="job.php"><img src="images/p-zoom-02.png"  alt=""/></a></li>
              </ul>
            </div>
          </div>
        </div><div class="partner">
          <div class="partner-thumb"> <a href="#"><img class="part-img" src="images/partner-5.jpg"  alt="Partner"/></a>
            <div class="partner-hover">
              <h4>Audiojungle</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a.</p>
              <p><strong>Industry:</strong> Audio, Sound, Music</p>
            </div>
          </div>
          <div class="partners-meta">
            <div class="partner-jobs"> Jobs: 200 </div>
            <div class="nav-buttons">
              <ul>
                <li class="search"><a><img src="images/p-zoom.png"  alt=""/></a></li>
                <li class="link"><a href="job.php"><img src="images/p-zoom-02.png"  alt=""/></a></li>
              </ul>
            </div>
          </div>
        </div><div class="partner">
          <div class="partner-thumb"> <a href="#"><img class="part-img" src="images/partner-6.jpg"  alt="Partner"/></a>
            <div class="partner-hover">
              <h4>Audiojungle</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a.</p>
              <p><strong>Industry:</strong> Audio, Sound, Music</p>
            </div>
          </div>
          <div class="partners-meta">
            <div class="partner-jobs"> Jobs: 200 </div>
            <div class="nav-buttons">
              <ul>
                <li class="search"><a><img src="images/p-zoom.png"  alt=""/></a></li>
                <li class="link"><a href="job.php"><img src="images/p-zoom-02.png"  alt=""/></a></li>
              </ul>
            </div>
          </div>
        </div><div class="partner">
          <div class="partner-thumb"> <a href="#"><img class="part-img" src="images/partner-7.jpg"  alt="Partner"/></a>
            <div class="partner-hover">
              <h4>Audiojungle</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a.</p>
              <p><strong>Industry:</strong> Audio, Sound, Music</p>
            </div>
          </div>
          <div class="partners-meta">
            <div class="partner-jobs"> Jobs: 200 </div>
            <div class="nav-buttons">
              <ul>
                <li class="search"><a><img src="images/p-zoom.png"  alt=""/></a></li>
                <li class="link"><a href="job.php"><img src="images/p-zoom-02.png"  alt=""/></a></li>
              </ul>
            </div>
          </div>
        </div><div class="partner">
          <div class="partner-thumb"> <a href="#"><img class="part-img" src="images/partner-1.jpg"  alt="Partner"/></a>
            <div class="partner-hover">
              <h4>Audiojungle</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a.</p>
              <p><strong>Industry:</strong> Audio, Sound, Music</p>
            </div>
          </div>
          <div class="partners-meta">
            <div class="partner-jobs"> Jobs: 200 </div>
            <div class="nav-buttons">
              <ul>
                <li class="search"><a><img src="images/p-zoom.png"  alt=""/></a></li>
                <li class="link"><a href="job.php"><img src="images/p-zoom-02.png"  alt=""/></a></li>
              </ul>
            </div>
          </div>
        </div><div class="partner">
          <div class="partner-thumb"> <a href="#"><img class="part-img" src="images/partner-2.jpg"  alt="Partner"/></a>
            <div class="partner-hover">
              <h4>Audiojungle</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a.</p>
              <p><strong>Industry:</strong> Audio, Sound, Music</p>
            </div>
          </div>
          <div class="partners-meta">
            <div class="partner-jobs"> Jobs: 200 </div>
            <div class="nav-buttons">
              <ul>
                <li class="search"><a><img src="images/p-zoom.png"  alt=""/></a></li>
                <li class="link"><a href="job.php"><img src="images/p-zoom-02.png"  alt=""/></a></li>
              </ul>
            </div>
          </div>
        </div><div class="partner">
          <div class="partner-thumb"> <a href="#"><img class="part-img" src="images/partner-3.jpg"  alt="Partner"/></a>
            <div class="partner-hover">
              <h4>Audiojungle</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a.</p>
              <p><strong>Industry:</strong> Audio, Sound, Music</p>
            </div>
          </div>
          <div class="partners-meta">
            <div class="partner-jobs"> Jobs: 200 </div>
            <div class="nav-buttons">
              <ul>
                <li class="search"><a><img src="images/p-zoom.png"  alt=""/></a></li>
                <li class="link"><a href="job.php"><img src="images/p-zoom-02.png"  alt=""/></a></li>
              </ul>
            </div>
          </div>
        </div><div class="partner">
          <div class="partner-thumb"> <a href="#"><img class="part-img" src="images/partner-4.jpg"  alt="Partner"/></a>
            <div class="partner-hover">
              <h4>Audiojungle</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a.</p>
              <p><strong>Industry:</strong> Audio, Sound, Music</p>
            </div>
          </div>
          <div class="partners-meta">
            <div class="partner-jobs"> Jobs: 200 </div>
            <div class="nav-buttons">
              <ul>
                <li class="search"><a><img src="images/p-zoom.png"  alt=""/></a></li>
                <li class="link"><a href="job.php"><img src="images/p-zoom-02.png"  alt=""/></a></li>
              </ul>
            </div>
          </div>
        </div><div class="partner">
          <div class="partner-thumb"> <a href="#"><img class="part-img" src="images/partner-5.jpg"  alt="Partner"/></a>
            <div class="partner-hover">
              <h4>Audiojungle</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue, suscipit a.</p>
              <p><strong>Industry:</strong> Audio, Sound, Music</p>
            </div>
          </div>
          <div class="partners-meta">
            <div class="partner-jobs"> Jobs: 200 </div>
            <div class="nav-buttons">
              <ul>
                <li class="search"><a><img src="images/p-zoom.png"  alt=""/></a></li>
                <li class="link"><a href="job.php"><img src="images/p-zoom-02.png"  alt=""/></a></li>
              </ul>
            </div>
          </div>
        </div>      </div>
      <div class="clear"></div>
    </div>
    <!-- /Content Inner --> 
    
  </div>
</div>
</div>
<!-- /Content --> 

<?php include ('footer.php'); ?>

</body>
</html>