<div id="header" class="inner" >
  <div class=""> 
    
    <!-- Logo -->
    <div id="logo"> <a href="index.php"><img src="images/Logo.png" width="205" height="50"  alt="logo"/></a><a class="menu-hider"></a></div>
    <!-- /Logo -->
    
      <ul id="navigation">
      <li class="current"> <a href="index.php">Home</a></li>
      <li class="first expanded"><a >Jobs</a>
        <ul class="submenu">
          <li><a href="jobs.php">Job listing</a></li>
          <li><a href="job.php">Job Details</a></li>
        </ul>
      </li>
      <li class="first expanded"><a >Candidates</a>
        <ul class="submenu">
          <li><a href="candidates-listing.php">Candidate Listing (with sidebar)</a></li>
          <li><a href="candidates-listing-no-sidebar.php">Candidate listing (without)</a></li>
          <li><a href="candidate.php">Candidate</a></li>
        </ul>
      </li>
      <li><a href="postajob.php">Post A Job</a></li>
      <li><a href="quickregister.php">Quick Register</a></li>
      <li><a href="partners.php">Partners</a></li>
      <li><a href="about-us.php">About Us</a></li>
      <li><a href="contacts.php">Contact</a></li>
    </ul>
    <div class="reponsive-nav">
   <select  class="select" name="menu1" id="menu1"> 
       <option value="index.php">Home</option>
      <option value="jobs.php">Job listing</option>
      <option value="job.php">Job Details</option>
      <option value="candidates-listing.php">Candidate Listing (with sidebar)</option>
      <option value="candidates-listing-no-sidebar.php">Candidate listing (without sidebar)</option>
      <option value="candidate.php">Candidate Details</option>
      <option value="partners.php">Partners</option>
      <option value="about-us.php">About Us</option>
      <option value="contacts.php">Contact</option>
    </select>
    </div>
  </div>
</div>
<!-- /Header --> 
