<?php 
session_start();

if(!isset($_SESSION['loggedin']))
  header("Location:index.php");
include('../config.php');
include('../queries.php');
?>
<!DOCTYPE html>
<html>
    <head>
	<title>AddToTable</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
             <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    </head>
    <body>
		<!-- top bar -->


		<!-- main content -->
		<div id="container">
			<div class="col-md-offset-2 col-md-8">
			
			
			
				<?php
				if(isset($_POST['pagename']))
					$actionpage=$_POST['pagename'];
				echo '<form class="form-horizontal"  action="'.$actionpage.'" method="post">';
				if(isset($_POST['fields']))
				{
					$fieldnames=$_POST['fields'];
					$tablename=$_POST['tablename'];
					$id=$_POST['id'];
					
				}
				$n=count($fieldnames);
				$max=maxm($tablename,$id);
				$max+=1;
				for($i=0;$i<$n;$i++)
				{
					echo '
					<div class="form-group">
					
					<label class="control-label col-md-3" style="text-align:left;" for="'.$i.'">'.$fieldnames[$i].'</label>';
					if($id==$fieldnames[$i])
					echo 
				    '<div class="col-md-9">
					<input class="form-control  " disabled type="text" id="'.$i.'" name="" value="'.$max.'">
					</div>';
					else
					echo '<div class="col-md-9">
					<input class="form-control" type="text " id="'.$i.'" name="columnvalues[]" value="">
					</div>
					<input type="hidden" name="columnnames[]" value="'.$fieldnames[$i].'">';

					echo '</div>';
					
				}
				echo '<input type="hidden" name="tablename" value="'.$tablename.'">';
				echo '<input type="hidden" name="primarykey" value="'.$id.'">';
				echo '<input type="submit"  class="btn btn-success" name="insert" value="Add To '.$tablename.'">';
				echo '</form>';
				?>

			
		</div>
			
		
		</div>
	
    </body>
</html>
