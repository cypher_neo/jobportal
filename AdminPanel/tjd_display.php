<?php 
session_start();

if(!isset($_SESSION['loggedin']))
  header("Location:index.php");
include('../config.php');
include('../queries.php');
if(isset($_POST['title']))
	$title=$_POST['title'];
if(isset($_POST['tablename']))
	$tablename=$_POST['tablename'];
if(isset($_POST['primarykey']))
	$primarykey=$_POST['primarykey'];
?>

<!DOCTYPE html>

<html lang="en"> 
    <head>
        <meta charset="UTF-8" />
     
        <title>ManageTable</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
             <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		
    </head>
    <body>

		<?php 
		
		$rec_limit=10;
		$rec_count=totalrecords($tablename);
		if( isset($_GET{'page'} ) )
         {
            $page = $_GET{'page'} + 1;
            $offset = $rec_limit * $page ;
         }
         else
         {
            $page = 0;
            $offset = 0;
         }
         $left_rec = $rec_count - ($page * $rec_limit);
         $result=select($tablename,'','',$rec_limit,$offset);
		$n=mysql_num_fields($result);

		for($i=0;$i<$n;$i++)
		{
			$fieldnames[]=mysql_field_name($result, $i);
		}
		//print_r($fieldnames);
		?>
		
		<!-- main content -->
		<div id="main_wrapper">
			<div class="page_bar clearfix">
				<div class="row">
					<div class="col-md-10">
						<h4 class="page_title">Manage <?php echo $tablename; ?></h4>
						<p class="text-muted">Total Rows: <?php echo $rec_count;?> </p>
					</div>
					<div class="col-md-2 text-right">
						<form action="add.php" method="post">
						<?php
						
						foreach($fieldnames as $names)
							echo '<input type="hidden" name="fields[]" value="'.$names.'">';
						echo '<input type="hidden" name="tablename" value="'.$tablename.'">';
						echo '<input type="hidden" name="id" value="'.$primarykey.'">';
						echo '<input type="hidden" name="pagename" value="display.php">';
						echo '<input type="submit" class="btn btn-success" value="Add '.$tablename.'">';
						
						?>
						</form>
					</div>
				</div>
			</div>
			<div class="page_content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-12">
							<div class="panel panel-default" style="overflow:auto; height:500px;">
								
								<?php
		if(isset($_POST['insert'])&& isset($_POST['columnnames'])&& isset($_POST['columnvalues']))
		{
			$formdata=array();
			$columnnames=$_POST['columnnames'];
			$columnvalues=$_POST['columnvalues'];
			$tablename=$_POST['tablename'];
			$n=count($columnnames);
			for($i=0;$i<$n;$i++)
			{
				$formdata[$columnnames[$i]]=$columnvalues[$i];

			}
			
			//print_r($whereClause);
			$inserted=insert($tablename,$formdata);
			if(!$inserted)	
				echo 'Error'.mysql_error();
			else
				echo '<div class="alert alert-success">
  <strong>Successfully Added to database!</strong> </div>';
				
			
		} 
		?>

		<?php

		//To verify a job post
			if(isset($_POST['tjid']))
			{
				$tjid=$_POST['tjid'];
			    $copied=copyTJDtoJD($tjid);
			    echo 'copied:'.$copied;
			    if($copied)
			    {	
			    	$whereClause=array($primarykey=>$tjid);
			    	$deleted=dbRowDelete($tablename,$whereClause);
			    	if($deleted)
			    	{
			    		echo '<div class="alert alert-success">
  <strong>Verified!</strong> </div>';
			    	}
			    	else
			    	{
			    	//echo 'inside delete error';
			    	 echo 'DeleteError'.mysql_error();

			    	}
			    }
			    else
			    {
			    echo 'CopyingError'.mysql_error();

			    }

			}

		?>


								<div class="text-center">
									<ul class="pagination pagination-sm">';
								<?php			
								if( $page > 0 )
						         {
						            $last = $page - 2;
						            echo '<li><a href='.$_PHP_SELF.'?page='.$last.'>
						            <i class="fa fa-angle-double-left"></i> Last 10 Records</a></li>';
						            echo '<li><a href='.$_PHP_SELF.'?page='.$page.'>Next 10 Records
						            <i class="fa fa-angle-double-right"></i></a></li>';
						         }
								         
						         else if( $page == 0 )
						         {
						            echo '<li><a href='.$_PHP_SELF.'?page='.$page.'>Next 10 Records
						            <i class="fa fa-angle-double-right"></i></a></li>';
								 }
									
						         else if( $left_rec < $rec_limit )
						         {
						            $last = $page - 2;
						            echo '<li><a href='.$_PHP_SELF.'?page='.$last.'>Last 10 Records
						            <i class="fa fa-angle-double-left"></i></a></li>';
						         }
						         ?>			
										</ul>
									</div>
									



								<div class="table-responsive">
									<table class="table info_table" id="prod_table">
										<thead>
											<tr>
																				
												<?php
												
												
												
												for($i=0;$i<$n;$i++)
												{
													
												echo 
												'<th class="sub_col">'.$fieldnames[$i].'</th>';
												}

												echo 
												'
												<th></th>
												<th></th>
											</tr>
										</thead>';
										
											
										while($rows=mysql_fetch_array($result,MYSQL_BOTH))
											{  
												//$fields=array();
												$fieldvalues=array();
											echo'
										<tr>';
											
											for($i=0;$i<$n;$i++)
											{	//$fields[$fieldnames[$i]]=$rows[$i];
												$fieldvalues[]=$rows[$i];
												echo '
											<td class="sub_col">'.$rows[$i].'</td>';
											}
											//$dataString=serialize($fields);
											echo '
												<form method="post" action="edit.php">';
												foreach($fieldnames as $names)
												echo 
											'<input type="hidden" name="fields[]" value="'.$names.'">';

												foreach($fieldvalues as $value)
												echo 
											'<input type="hidden" name="values[]" value="'.$value.'">';
											echo '<input type="hidden" name="tablename" value="'.$tablename.'">';
											echo '<input type="hidden" name="id" value="'.$primarykey.'">';
											echo '
												<td class="sub_col"><input class="btn btn-default btn-sm"
												 type="submit" value="Edit"> </td>
												
												</form>
												<form method="post" action="tjd_display.php">
													<td class="sub_col"><input class="btn btn-default btn-sm"
													 type="submit" name="verify" value="Verify"> </td>
													 <input type="hidden" name="tjid" value="'.$rows['TmpJobID'].'">
													 <input type="hidden" name="tablename" value="'.$tablename.'">
													 <input type="hidden" name="primarykey" value="'.$primarykey.'">
												 </form>



										</tr>';
											}
								echo '		
									</table>
								</div>';




								?>
							</div>
						</div>
					</div>
				</div>
			</div>
					
		</div>
</body>
</html>
	

		
