<?php 
session_start();

if(!isset($_SESSION['loggedin']))
  header("Location:index.php");
include('../config.php');
include('../queries.php');
?>
<!DOCTYPE html>
<html>
    <head>	
        <title>ManageTable</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
             <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
		
    </head>
    <body>

		


		<!-- main content -->
		<div class="container">
			<div class="col-md-offset-2 col-md-8">
				<?php
		if(isset($_POST['update'])&& isset($_POST['columnnames'])&& isset($_POST['columnvalues']))
		{
			$formdata=array();
			$columnnames=$_POST['columnnames'];
			$columnvalues=$_POST['columnvalues'];
			$tablename=$_POST['table'];
			$id=$_POST['id'];
			$n=count($columnnames);
			for($i=0;$i<$n;$i++)
			{
				$formdata[$columnnames[$i]]=$columnvalues[$i];

			}
			$whereClause=array($id=>$formdata[$id]);
			//print_r($whereClause);
			$result=update($tablename,$formdata,$whereClause);
			if(!$result)	
				echo 'Error'.mysql_error();
			else
				echo '<div class="alert alert-success">
  <strong>Successfully Updated!</strong> </div>';
				
			
		} 
		?>
			<form role="form" class="form-horizontal"  action="edit.php" method="post">
				<?php
				if(isset($_POST['fields'])&&isset($_POST['values']))
				{
					$fieldnames=$_POST['fields'];
					$fieldvalues=$_POST['values'];
					$tablename=$_POST['tablename'];
					$id=$_POST['id'];
					
				}
				elseif(isset($_POST['update'])&& isset($_POST['columnnames'])&& isset($_POST['columnvalues']))
				{
					$fieldnames=$_POST['columnnames'];
					$fieldvalues=$_POST['columnvalues'];
					$tablename=$_POST['table'];
				}

				$n=count($fieldnames);
				for($i=0;$i<$n;$i++)
				{
					echo '
					<div class="form-group">
					
					<label class="control-label col-md-3 " style="text-align:left;" for="'.$i.'">'.$fieldnames[$i].':</label>
					<div class="col-md-9">
					<input class="form-control " type="text" id="'.$i.'" name="columnvalues[]" value="'.$fieldvalues[$i].'">
					</div>
					<input type="hidden" name="columnnames[]" value="'.$fieldnames[$i].'">

					</div>';
				}
				echo '<input type="hidden" name="table" value="'.$tablename.'">';
				echo '<input type="hidden" name="id" value="'.$id.'">';
				echo '<input type="submit" class="btn btn-success" name="update" value="Update">';
				echo '&nbsp;&nbsp;&nbsp;';
				echo '<a class="btn btn-success" href="home.php">GO BACK!</a>';
				?>

			</form>
		</div>	
		</div>

    </body>
</html>
