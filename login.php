
<!DOCTYPE html>
<?php include('config.php');?>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6 lt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7 lt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8 lt8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="UTF-8" />
     
        <title>Login/Signup</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <link rel="shortcut icon" href="../favicon.ico"> 
        <link rel="stylesheet" type="text/css" href="css/customcss/demo.css" />
        <link rel="stylesheet" type="text/css" href="css/customcss/style.css" />
	<link rel="stylesheet" type="text/css" href="css/customcss/animate-custom.css" />
		
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Lato:300,400,700&amp;subset=latin,latin-ext"/>
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
<link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-responsive.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/reset.css"/>
<link id="color_css" rel="stylesheet" type="text/css" href="css/color_scheme_1.css"/>
<link rel="stylesheet" type="text/css" href="css/jquery.combosex.css"/>
<link rel="stylesheet" type="text/css" href="css/jquery.flexslider.css"/>
<link rel="stylesheet" type="text/css" href="css/jquery.scrollbar.css"/>

<!--[if (lte IE 9)]>
    <link rel="stylesheet" type="text/css" href="css/iefix.css"/>
    <![endif]-->
<script type="text/javascript" src="js/jquery.1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.1.7.2.min.js"></script>
<script type="text/javascript" src="js/jquery.combosex.min.js"></script>
<script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="js/jquery.easytabs.min.js"></script>
<script type="text/javascript" src="js/jquery.gmap.min.js"></script>
<script type="text/javascript" src="js/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="js/jQuery.menutron.js"></script>
<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script src="js/gen_validatorv4.js" type="text/javascript"></script>
		
    </head>
    <body>
    
        <?php include('topheader.php')?>
        <?php include('header.php')?>
              <div  style="height:100px;"></div>
        <div class="container">  
            <section >				
                <div id="container_demo" >
                    <!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
                    <a class="hiddenanchor" id="toregister"></a>
                    <a class="hiddenanchor" id="tologin"></a>
                    <div id="wrapper">
                        <div id="login" class="animate form">
                            <form  action="register.php" autocomplete="on" method="post"> 
                                <h1>Log in</h1>
                                <p>
                                <label for="utype"  >I am a: </label>
                                 
                                 <select id="utype" name="usertype">
                                      <option value="">-----</option>
                                      <option value="Job Seeker">Job Seeker</option>
                                      <option value="Recruiter">Recruiter</option>
                                </select> 
                                </p>
                                <p> 
                                    <label for="username" class="uname" data-icon="u" > Your email </label>
                                    <input id="username" name="email" required="required" type="email" placeholder="EmailId"/>
                                </p>
                                <p> 
                                    <label for="password" class="youpasswd" data-icon="p"> Your password </label>
                                    <input id="password" name="password" required="required" type="password" placeholder="********" /> 
                                </p>
                                <p class="login button"> 
                                    <input type="submit" name="login_submit" value="Login" /> 
								</p>
                                <p class="change_link">
									Not a member yet ?
									<a href="#toregister" class="to_register">Join us</a>
								</p>
                            </form>
                        </div>
                     <div  style="height:100px;"></div>
                        <div id="register" class="animate form">
                            <form  id="signup" action="register.php" autocomplete="off" method="post"> 
                                <h1> Sign up </h1> 
                                <p>
                                <label for="s_utype"  >I am a: </label>
                                 
                                 <select onchange="onchangeShow()" id="s_utype" name="usertype">
                                      <option value="">-----</option>
                                      <option value="Job Seeker">Job Seeker</option>
                                      <option value="Recruiter">Recruiter</option>
                                </select> 
                                </p>

                                <p>
                                    <label for="namesignup" class="youmail" data-icon="n" > Your Name</label>
                                    <input id="namesignup" name="name" required="required" type="text" placeholder="Humans have names."/> 
                                </p>
                                <p> 
                                    <label for="emailsignup" class="youmail" data-icon="e" > Your email</label>
                                    <input id="emailsignup" name="email" required="required" type="email" placeholder="mysupermail@mail.com"/> 
                                </p>
                                <p id="p_cname" style="display:none;" >
                                    <label for="cname" class="youmail" data-icon="c" > Company Name</label>
                                    <input id="cname" name="companyname" required="required" type="text" placeholder="Name of the Company."/> 
                                </p>
                                <p id="p_cindustry" style="display:none;" >
                                    <label for="cindustry" class="youmail" data-icon="I" > Company Industry</label>
                                    <input id="cindustry" name="companyindustry" required="required" type="text" placeholder="e.g IT,Marketing,etc."/> 
                                </p>
                                <p> 
                                    <label for="passwordsignup" class="youpasswd" data-icon="p">Your password </label>
                                    <input id="passwordsignup" name="password" required="required" type="password" placeholder="********"/>
                                </p>
                                <p> 
                                    <label for="passwordsignup_confirm" class="youpasswd" data-icon="p">Please confirm your password </label>
                                    <input id="passwordsignup_confirm" name="cnf_password" required="required" type="password" placeholder="********"/>
                                </p>
                                <p class="signin button"> 
									<input type="submit" name="signup_submit" value="Sign up"/> 
								</p>
                                <p class="change_link">  
									Already a member ?
									<a href="#tologin" class="to_register"> Go and log in </a>
								</p>
                            </form>
                        </div>
						
                    </div>
                </div>  
            </section>
        </div>
           <?php include('footer.php')?>
<script type="text/javascript">
function MatchPasswords()
{
  var frm = document.forms["signup"];
  if(frm.password.value != frm.cnf_password.value)
  {
    sfm_show_error_msg('Passwords do not match!',frm.cnfpassword);
    return false;
  }
  else
  {
    return true;
  }
}
function onchangeShow() {
    var id= document.getElementById("s_utype");
    var selected = id.options[id.selectedIndex].value;
    if(!"Job Seeker".localeCompare(selected))
    {   
        document.getElementById("p_cname").style.display="none";
        document.getElementById("p_cindustry").style.display="none";
        document.getElementById("cname").disabled=true;
        document.getElementById("cindustry").disabled=true;
    }
    else if(!"Recruiter".localeCompare(selected))
    { 
        document.getElementById("p_cname").style.display="block";
        document.getElementById("p_cindustry").style.display="block";
        document.getElementById("cname").disabled=false;
        document.getElementById("cindustry").disabled=false;

    }

}

var validator=new Validator("signup");
validator.addValidation("name","req","Please enter your First Name");
validator.addValidation("email","req","Please enter your email address");
validator.addValidation("email","email");
validator.addValidation("password","req","Password field can't be left blank");
validator.setAddnlValidationFunction(MatchPasswords);

</script>
    </body>
</html>